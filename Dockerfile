# this is an official Python runtime, used as the parent image
FROM python:3.10.4

# set the working directory in the container to /app
WORKDIR /app

COPY . /app

RUN pip install --trusted-host pypi.python.org -r requirements.txt

# 5000 is for documentation purpose only
# only exposing when starting with --publis-all command https://docs.docker.com/engine/reference/commandline/run/
EXPOSE 5000

# execute the app
CMD ["python", "run.py"]
