from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed
from flask_login import current_user
from wtforms import HiddenField, StringField, PasswordField, SubmitField, BooleanField, TextAreaField, SelectField, IntegerField, validators
from wtforms.validators import DataRequired, Length, Email, EqualTo, ValidationError, Regexp
from designgarage.models import user
from designgarage.db import db_users
import re

class RegistrationForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired(), Length(min=2, max=20)])
    email = StringField('Email', validators=[DataRequired(), Email(message="Invalid email format")])
    password = PasswordField('Password', validators=[DataRequired()])
    confirm_password = PasswordField('Confirm Password',validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Sign Up')
    
    def validate_username(self, username):
        user = db_users.get_user_by_username(username=username.data)
        if user:
            raise ValidationError('That username is already in use, please choose a different.')
        
    def validate_email(self, email):
        user = db_users.get_user_by_email(email=email.data)
        if user:
            raise ValidationError('That email address is already in use, please choose a different.')


class LoginForm(FlaskForm):
    email = StringField('Email',validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember = BooleanField('Remember Me')
    submit = SubmitField('Login')
    

class UpdateAccountForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired(), Length(min=2, max=20)])
    email = StringField('Email', validators=[DataRequired(), Email(), Regexp(r'^[\w\.-]+@[\w\.-]+\.[a-zA-Z]{2,}(?![\d.])', message="Invalid email address format")])
    new_email = StringField('New Email', validators=[DataRequired(), Email(), Regexp(r'^[\w\.-]+@[\w\.-]+\.[a-zA-Z]{2,}(?![\d.])$', message="Invalid email address format")])
    picture = FileField('Update Profile Picture', validators=[FileAllowed(['jpg', 'png'])])
    password = PasswordField('Password', validators=[DataRequired(), Length(min=4, max=20)])
    submit = SubmitField('Update')
    
    def validate_username(self, username):
        if username.data != current_user.username:
            user = db_users.get_user_by_username(username=username.data)
            if user:
                raise ValidationError('That username is already in use, please choose a different.')
        
    def validate_email(self, email):
        if email.data != current_user.email:
            user = db_users.get_user_by_email(email=email.data)
            if user:
                raise ValidationError('That email address is already in use, please choose a different.')
            if not re.match(r"[^@]+@[^@]+\.[^@]+", email):
                raise ValidationError('Invalid email address format. Please enter a valid email address.')
                
class ProductForm(FlaskForm):
    title = StringField('Product name', validators=[DataRequired()])
    brandname = StringField('Product brand', validators=[])
    category = StringField('Product categorty', validators=[])
    content = TextAreaField('Product details',validators=[DataRequired(), Length(max=500)])
    price = IntegerField('Price',validators=[DataRequired()])
    picture = FileField('Add Product Picture', validators=[FileAllowed(['jpg', 'png'])]) 
    submit = SubmitField ('Post new product')
    
class UpdateProductForm(FlaskForm):
    title = StringField('Product name', validators=[DataRequired()])
    brandname = StringField('Product brand', validators=[])
    category = StringField('Product categorty', validators=[])
    content = TextAreaField('Product details',validators=[DataRequired(),Length(max=500)])
    price = IntegerField('Price',validators=[DataRequired()])
    picture = FileField('Change Product Picture', validators=[FileAllowed(['jpg', 'png'])])
    submit = SubmitField ('Update product')

class DeleteProductForm(FlaskForm):
    product_id = HiddenField('Product ID', validators=[DataRequired()])
    submit = SubmitField ('Delete product')

class ChangePassword(FlaskForm):
    old_password = PasswordField('Old Password', validators=[])
    new_password = PasswordField('New Password', validators=[DataRequired(), Length(min=8, message='Password must be at least 8 characters long')])
    confirm_password = PasswordField('Confirm New Password', validators=[DataRequired(),EqualTo('new_password', message='Passwords must match')])
    submit = SubmitField('Update Password')

class ResetPassword(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Email()])
    submit = SubmitField('Reset Password')

    def validate_email(self, email):
        user = db_users.get_user_by_email(email=email.data)
        if not user:
            raise ValidationError('Email address was not found.')
