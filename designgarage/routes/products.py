# Standard library imports
import os
import secrets

# Related third-party imports
from PIL import Image
from flask import abort, flash, jsonify, redirect, render_template, render_template_string, request, url_for
from flask_bcrypt import Bcrypt
from flask_login import current_user, login_required, login_user, logout_user
from .pages import setup_page_routes
from . import is_active_page

# Local application/library-specific imports
from designgarage import app, common
from designgarage.db import db as Db, db_passwordresetcodes, db_products, db_users
from designgarage.forms import RegistrationForm, LoginForm, ResetPassword, UpdateAccountForm, ProductForm, UpdateProductForm, DeleteProductForm, ChangePassword
from designgarage.models.product import Product

def save_productpicture(form_picture) -> str:
    random_hex = secrets.token_hex(8) #Randomizing the image name
    _, f_ext = os.path.splitext(form_picture.filename)
    picture_fn = random_hex + f_ext
    picture_path = os.path.join(app.root_path, 'static/product_pics', picture_fn) #this gives the rootpath to save the images

    output_size = (640, 640) #resizing the image, check out a video on Pillow package for images
    i = Image.open(form_picture)
    i.thumbnail(output_size)
    i.save(picture_path)

    return picture_fn

def setup_product_routes(app):    
    @app.route("/products", methods=['GET'])
    def products():
        all_products = db_products.get_products()
        productpic_base_url = url_for('static', filename='product_pics')
        product_base_url = url_for('products')
        return render_template('products.html', products = all_products, productpic_base_url = productpic_base_url, product_base_url = product_base_url, is_active_page=is_active_page)


    @app.route("/products/new", methods=['GET', 'POST'])
    @login_required
    def new_product():
        form = ProductForm() #adding the form from the forms.py to here, and then passing it so it is rendering it out when we call this 1
        if form.validate_on_submit():
            product = db_products.create_product_by_properties(common.generate_uuid(),form.title.data, current_user.username, form.brandname.data, form.category.data, form.content.data, form.price.data, common.generate_created_at(),"" )
            if form.picture.data:
                picture_file = save_productpicture(form.picture.data)
                db_products.change_product_picture_path(product.id, picture_file)
            else:
                picture_file = url_for('static', filename='product_pics/default_product.png')
            flash('Your product has been created', "success")
            return redirect(url_for('products'))

        return render_template('product_create.html', title='New Product', form = form, is_active_page=is_active_page, legend='New Product')



    @app.route("/products/<string:product_id>", methods=['GET', 'POST'])
    @login_required
    def single_product(product_id):
        product = db_products.get_product_by_id(product_id)
        create_form = ProductForm()
        delete_form = DeleteProductForm()
        productpic_base_url = url_for('static', filename='product_pics')
        product_base_url = url_for('products')
        # showing the product page itself
        if request.method == 'GET':
            return render_template('singleproduct.html',productpic_base_url = productpic_base_url, product_base_url = product_base_url, create_form=create_form, delete_form=delete_form, product = product, is_active_page=is_active_page, legend='Update Post')    

        elif request.method ==  'POST':
            return #TODO



    @app.route("/product/update/<string:product_id>", methods=['GET', 'POST'])
    @login_required
    def update_product(product_id):
        product = db_products.get_product_by_id(product_id)
        form = UpdateProductForm()
        picture_file = ""
        if form.validate_on_submit():
            new_content = form.content.data
            new_title = form.title.data
            new_price = form.price.data
            new_category = form.category.data
            new_brandname = form.brandname.data
            if form.picture.data:
                picture_file = save_productpicture(form.picture.data)
                db_products.change_product_picture_path(product_id, picture_file)
            flash('Your picture updated!', 'success')
            db_products.change_product_title(product_id, new_title)
            db_products.change_product_content(product_id,new_content)
            db_products.change_product_price(product_id,new_price)
            db_products.change_product_category(product_id,new_category)
            db_products.change_product_brandname(product_id,new_brandname)
            flash('Your product has been updated!', 'success')
            return redirect(url_for('single_product', product_id=product_id))

        elif request.method == 'GET':
            form.title.data = product.title
            form.content.data = product.content
            form.price.data = product.price
            form.picture.data = picture_file
            form.brandname.data = product.brandname
            form.category.data = product.category
        if product.picturepath:
            image_file = url_for('static', filename='product_pics/' + product.picturepath)
        else:
            image_file = url_for('static', filename='product_pics/default_product.png')
        return render_template('product_update.html', title='Product update', picturepath = image_file , form=form, product = product, is_active_page=is_active_page)



    @app.route("/products/delete/<string:product_id>", methods=['POST'])
    @login_required
    def delete_product(product_id):
        product = db_products.get_product_by_id(product_id)
        if product.username != current_user.username:
            abort(403)
        issuccesfull = db_products.delete_product(product_id)
        if issuccesfull:
            flash ('Your product has been deleted', 'success')
            return redirect(url_for('products'))
        return 
