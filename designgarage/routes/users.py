# Standard library imports
import os
import secrets

# Related third-party imports
from PIL import Image
from flask import abort, flash, jsonify, redirect, render_template, render_template_string, request, url_for
from flask_bcrypt import Bcrypt
from flask_login import current_user, login_required, login_user, logout_user
from .pages import setup_page_routes
from . import is_active_page


# Local application/library-specific imports
from designgarage import app, common
from designgarage.db import db as Db, db_passwordresetcodes, db_products, db_users
from designgarage.forms import RegistrationForm, LoginForm, ResetPassword, UpdateAccountForm, ProductForm, UpdateProductForm, DeleteProductForm, ChangePassword
from designgarage.models.product import Product

def save_profilepicture(form_picture) -> str:
    print("Saving profile picture...")
    random_hex = secrets.token_hex(8) #Randomizing the image name
    _, f_ext = os.path.splitext(form_picture.filename)
    picture_fn = random_hex + f_ext

    #this gives the rootpath to save the images
    picture_path = os.path.join(app.root_path, 'static/profile_pics', picture_fn)

    #resizing the image, check out a video on Pillow package for images
    output_size = (300, 300)
    i = Image.open(form_picture)
    i.thumbnail(output_size)
    i.save(picture_path)
    print(f"Saved picture path: {picture_path}")
    return picture_fn

def setup_user_routes(app):
    @app.route("/account", methods=['GET', 'POST'])
    @login_required
    def account():
        form = UpdateAccountForm()
        form.username.data = current_user.username
        form.email.data = current_user.email
        form.password.data = current_user.password
        # Assuming picture_file should be retrieved from the database
        picture_file = current_user.picturepath or ""
        form.picture.data = picture_file

        if current_user.picturepath:
            image_file = url_for('static', filename='profile_pics/' + current_user.picturepath)
        else:
            image_file = url_for('static', filename='profile_pics/default_profile.png')

        return render_template('account.html', title='My Page', picturepath=image_file, user=current_user, form=form, is_active_page=is_active_page)

    @app.route("/account/update_email", methods=["POST"])
    @login_required
    def update_email():
        new_email = request.form.get("new_email")
        if not new_email:
            flash("Email field is required", "error")
            return redirect(url_for("account"))

        if db_users.change_user_email(current_user.username, new_email):
            flash("Email updated successfully", "success")
        else:
            flash("Failed to update email", "error")

        return redirect(url_for("account"))

    @app.route("/account/update_password", methods=["POST"])
    @login_required
    def update_password():
        new_password = request.form.get("new_password")
        if not new_password:
            flash("Password field is required", "error")
            return redirect(url_for("account"))

        if db_users.change_user_password(current_user.username, new_password):
            flash("Password updated successfully", "success")
        else:
            flash("Failed to update password", "error")

        return redirect(url_for("account"))

    @app.route("/account/update_picture", methods=["POST"])
    @login_required
    def update_picture():
        new_picture = request.files.get("new_picture")
        print("Received picture:", new_picture)
        if not new_picture:
            print("No picture uploaded")
            return jsonify({"error": "No picture uploaded"}), 400
    
        picture_file = save_profilepicture(new_picture)
        print("Saved picture file:", picture_file)
        if picture_file:
            if db_users.change_user_picture_path(current_user.username, picture_file):
                flash("Picture updated successfully", "success")
                print("Picture path updated in database")
            else:
                flash("Failed to update picture in database", "error")
                print("Failed to update picture path in database")
        else:
            flash("Failed to save uploaded picture", "error")
            print("Failed to save uploaded picture")
    
        return redirect(url_for("account"))
    


    @app.route("/passwordreset", methods=['GET', 'POST'])
    def reset_password():
        form = ResetPassword()
        if form.validate_on_submit():
            user = db_users.get_user_by_email(email=form.email.data)
            if not user:
                return redirect(url_for('login')) 

            #mivel van ilyen email ezért csak a passwordot csekkoljuk ezután
            randomcode = common.generate_randomcode()
            db_passwordresetcodes.create_passwordresetcode(user.id, randomcode)
            common.send_mail(user.email, 
                            "Reset your password",
                            url_for("reset_password_for_user",
                            _external=True, 
                            username=user.username, code=randomcode))

            return redirect(url_for('login')) 

        return render_template('passwordreset.html', title='Password Reset', form=form, is_active_page=is_active_page)

    @app.route("/passwordreset/<string:username>", methods=['GET', 'POST'])
    def reset_password_for_user(username):
        form = ChangePassword()
        if request.method == 'GET':
            return render_template('password_update.html', title='Password Update', form=form, is_active_page=is_active_page)

        if request.method == 'POST':
            if form.validate_on_submit():
                inputcode = request.args.get('code', '')
                user=db_users.get_user_by_username(username)
                code=db_passwordresetcodes.get_code_for_user_by_username(user.id)

                if code.code == inputcode:
                    if form.new_password.data == form.confirm_password.data:
                        db_users.change_user_password(username, form.new_password.data)
                        db_passwordresetcodes.delete_passwordresetcode_by_userid(user.id)
                        return redirect(url_for('login'))
