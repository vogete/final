
import os
import secrets
from PIL import Image
from flask import app, request, url_for

def is_active_page(url_rule):
    return request.path == url_for(url_rule)
