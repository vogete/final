# Standard library imports
import os
import secrets

# Related third-party imports
from PIL import Image
from flask import abort, flash, jsonify, redirect, render_template, render_template_string, request, url_for
from flask_bcrypt import Bcrypt
from flask_login import current_user, login_required, login_user, logout_user
from .pages import setup_page_routes
from . import is_active_page

# Local application/library-specific imports
from designgarage import app, common
from designgarage.db import db as Db, db_passwordresetcodes, db_products, db_users
from designgarage.forms import RegistrationForm, LoginForm, ResetPassword, UpdateAccountForm, ProductForm, UpdateProductForm, DeleteProductForm, ChangePassword
from designgarage.models.product import Product

def setup_auth_routes(app):    
    @app.route("/register", methods=['GET', 'POST'])
    def register():
        if current_user.is_authenticated:
            return redirect(url_for('account'))
        form = RegistrationForm()
        if form.validate_on_submit():
            db_users.create_user_by_properties(common.generate_uuid(), form.username.data, "", "", form.email.data, form.password.data, common.generate_created_at())
            flash('Your account has been created. Welcome to the DesignGarage!', 'success')
            return redirect(url_for('login'))

        return render_template('register.html', title='Register', form=form, is_active_page=is_active_page)


    @app.route("/login", methods=['GET', 'POST'])
    def login():
        bcrypt = Bcrypt(app)
        error_message = None
        if current_user.is_authenticated: 
            return redirect(url_for('account'))
        form = LoginForm()
        if form.validate_on_submit():
            user = db_users.get_user_by_email(email=form.email.data)
            if user and bcrypt.check_password_hash(user.password, form.password.data):
                login_user(user, remember=form.remember.data)
                next_page = request.args.get('next')
                flash('You have been logged in!', 'success')
                return redirect(next_page) if next_page else redirect(url_for('home'))
            else:
                error_message = 'Login failed, wrong username or password'


        return render_template('login.html', title='Login', form=form, error_message=error_message, is_active_page=is_active_page)

    @app.route("/logout")
    def logout():
        logout_user()
        return redirect(url_for('home'))
