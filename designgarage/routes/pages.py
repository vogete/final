# Standard library imports
import os
import secrets

# Related third-party imports
from PIL import Image
from flask import abort, flash, jsonify, redirect, render_template, render_template_string, request, url_for
from flask_bcrypt import Bcrypt
from flask_login import current_user, login_required, login_user, logout_user

from . import is_active_page
# Local application/library-specific imports
from designgarage import app, common
from designgarage.db import db as Db, db_passwordresetcodes, db_products, db_users
from designgarage.forms import RegistrationForm, LoginForm, ResetPassword, UpdateAccountForm, ProductForm, UpdateProductForm, DeleteProductForm, ChangePassword
from designgarage.models.product import Product

def setup_page_routes(app):
    @app.errorhandler(500)
    def internal_error(error):
        return render_template('500.html'), 500

    @app.errorhandler(404)
    def page_not_found(error):
        return render_template('404.html',is_active_page=is_active_page)

    @app.route("/")
    @app.route("/home")
    def home():
        return render_template('home.html',is_active_page=is_active_page)


    @app.route('/search', methods=['GET'])
    def search():
        q = request.args.get("q", "").strip()
        page = request.args.get("page", 1, type=int)
        results_per_page = 8

        if q == "":
            # Return an empty div
            return render_template_string('<div id="results"></div>')

        if not q:
            return jsonify({"error": "Query parameter 'q' is required."}), 400

        products, total_pages = db_products.search_products(q, page, results_per_page)

        return render_template("search_results.html", results=products, is_active_page=is_active_page, current_page=page, total_pages=total_pages)


    @app.route('/about')
    def about():
        return render_template('about.html', is_active_page=is_active_page)