"""
Database queries that is related to products.
"""

# All these imported modules are coded in this project
from designgarage.models.product import Product
import designgarage.common as common
import designgarage.db.db as Db


def get_products():
    """Get all products. Returns a `List[product]` object."""
    try:
        # Connect to database
        db = Db._db_connect(common.DB_NAME)
        # Create a cursor that will execute a query
        cur = db.cursor()
        # Execute query (using the cursor)
        cur.execute("SELECT * FROM products ORDER BY created DESC;")
        # Fetch all data (from the cursor)
        results = cur.fetchall()

        # If there are no results, we just return an empty List
        if not results:
            print("No results found")
            return []

        # Create an empty list where we store all results
        products = []
        # Go through all results, and create product (from `/models/product.py`) object
        for result in results:
            # Creating  product object
            product = Product(
                id = result["id"],
                title= result["title"],
                username = result["username"],
                brandname = result["brandname"],
                category= result ["category"],
                content = result["content"],
                price = result["price"],
                picturepath = result['picture_path'],
                created = result["created"],
            )
            
            # Add this product to the List
            products.append(product)

        # Return the list of users
        return products

    # If any error happened, then just return an empty list, and print the error
    except Exception as e:
        print(e)
        return []


def get_products_for_user_by_username(username: str):
    """Get all products for a single User. Returns a `List[product]` object."""
    try:
        # Connect to database
        db = Db._db_connect(common.DB_NAME)
        # Create a cursor that will execute a query
        cur = db.cursor()
        # Execute query (using the cursor)
        cur.execute(
            "SELECT * FROM products WHERE username=? ORDER BY created DESC;", (username,)
        )
        # Fetch all data (from the cursor)
        results = cur.fetchall()

        # If there are no results, we just return an empty List
        if not results:
            print("No results found")
            return []

        # Create an empty list where we store all results
        products = []
        # Go through all results, and create a User (from `/models/product.py`) object
        for result in results:
            # Creating User object
            product = Product(
                id = result["id"],
                title= result["title"],
                username = result["username"],
                brandname = result["brandname"],
                category= result ["category"],
                content = result["content"],
                price = result["price"],
                picturepath = result['picture_path'],
                created = result["created"],
            )
            # Add this user to the List
            products.append(product)

        # Return the list of users
        return products

    # If any error happened, print the error and return None.
    except Exception as e:
        print(e)
        return None


def get_product_by_id(product_id: str):
    """Get a single product by product_id. Returns a `Product` object."""
    try:
        db = Db._db_connect(common.DB_NAME)
        cur = db.cursor()
        # Run query
        cur.execute("SELECT * FROM products WHERE id=?", (product_id,))
        # Only fetch one single result, because ids should be unique.
        result = cur.fetchone()

        # If there are no results, we just return None
        if not result:
            print(f"No product found for {product_id}")
            return None

        # Create a product object with all the details
        product = Product(
            id = result["id"],
            title= result["title"],
            username = result["username"],
            brandname = result["brandname"],
            category= result ["category"],
            content = result["content"],
            price = result["price"],
            picturepath = result['picture_path'],
            created = result["created"],
            )
        # Return the product object
        return product
    # If any error happened, print the error and return None.
    except Exception as e:
        print(e)
        return None
    
def get_product_by_title(product_title: str):
    """Get a single user by Username. Returns a `User` object."""
    try:
        db = Db._db_connect(common.DB_NAME)
        cur = db.cursor()
        # Run query
        cur.execute("SELECT * FROM products WHERE title=?", (product_title,))
        # Only fetch one single result, because ids should be unique.
        result = cur.fetchone()

        # If there are no results, we just return None
        if not result:
            print(f"No product found for {product_title}")
            return None

        # Create a product object with all the details
        product = Product(
            id = result["id"],
            title= result["title"],
            username = result["username"],
            brandname = result["brandname"],
            category= result ["category"],
            content = result["content"],
            price = result["price"],
            picturepath = result['picture_path'],
            created = result["created"],
            )
        # Return the product object
        return product
    # If any error happened, print the error and return None.
    except Exception as e:
        print(e)
        return None

def get_product_by_content(product_content: str):
    """Get a single user by Username. Returns a `User` object."""
    try:
        db = Db._db_connect(common.DB_NAME)
        cur = db.cursor()
        # Run query
        cur.execute("SELECT * FROM products WHERE content=?", (product_content,))
        # Only fetch one single result, because ids should be unique.
        result = cur.fetchone()

        # If there are no results, we just return None
        if not result:
            print(f"No product found for {product_content}")
            return None

        # Create a product object with all the details
        product = Product(
            id = result["id"],
            title= result["title"],
            username = result["username"],
            brandname = result["brandname"],
            category= result ["category"],
            content = result["content"],
            price = result["price"],
            picturepath = result['picture_path'],
            created = result["created"],
            )
        # Return the product object
        return product
    # If any error happened, print the error and return None.
    except Exception as e:
        print(e)
        return None
    
    
def get_product_by_brandname(product_brandname: str):
    """Get a single user by Username. Returns a `User` object."""
    try:
        db = Db._db_connect(common.DB_NAME)
        cur = db.cursor()
        # Run query
        cur.execute("SELECT * FROM products WHERE brandname=?", (product_brandname,))
        # Only fetch one single result, because ids should be unique.
        result = cur.fetchone()

        # If there are no results, we just return None
        if not result:
            print(f"No product found for {product_brandname}")
            return None

        # Create a product object with all the details
        product = Product(
            id = result["id"],
            title= result["title"],
            username = result["username"],
            brandname = result["brandname"],
            category= result ["category"],
            content = result["content"],
            price = result["price"],
            picturepath = result['picture_path'],
            created = result["created"],
            )
        # Return the product object
        return product
    # If any error happened, print the error and return None.
    except Exception as e:
        print(e)
        return None

def get_product_by_category(product_category: str):
    """Get a single user by Username. Returns a `User` object."""
    try:
        db = Db._db_connect(common.DB_NAME)
        cur = db.cursor()
        # Run query
        cur.execute("SELECT * FROM products WHERE category=?", (product_category,))
        # Only fetch one single result, because ids should be unique.
        result = cur.fetchone()

        # If there are no results, we just return None
        if not result:
            print(f"No product found for {product_category}")
            return None

        # Create a product object with all the details
        product = Product(
            id = result["id"],
            title= result["title"],
            username = result["username"],
            brandname = result["brandname"],
            category= result ["category"],
            content = result["content"],
            price = result["price"],
            picturepath = result['picture_path'],
            created = result["created"],
            )
        # Return the product object
        return product
    # If any error happened, print the error and return None.
    except Exception as e:
        print(e)
        return None
    
def get_product_picturepath(product_picturepath: str):
    """Get a single user by Username. Returns a `User` object."""
    try:
        db = Db._db_connect(common.DB_NAME)
        cur = db.cursor()
        # Run query
        cur.execute("SELECT * FROM products WHERE picturepath=?", (product_picturepath,))
        # Only fetch one single result, because ids should be unique.
        result = cur.fetchone()

        # If there are no results, we just return None
        if not result:
            print(f"No product found for {product_picturepath}")
            return None

        # Create a product object with all the details
        product = Product(
            id = result["id"],
            title= result["title"],
            username = result["username"],
            brandname = result["brandname"],
            category= result ["category"],
            content = result["content"],
            price = result["price"],
            picturepath = result['picture_path'],
            created = result["created"],
            )
        # Return the product object
        return product
    # If any error happened, print the error and return None.
    except Exception as e:
        print(e)
        return None

def create_product(product: Product):
    """Create a product in the database based on a `product` object. Returns `True` if successful, `False` if it failed."""
    try:
        # Connect to database
        db = Db._db_connect(common.DB_NAME)
        # Get the cursor (that will execute the query)
        cur = db.cursor()
        # Execute query with the values from the User object.
        cur.execute(
            "INSERT INTO products (id, title, username, brandname, category, content, price, picture_path, created) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
            (product.id, product.title, product.username, product.brandname, product.category, product.content, product.price, product.picturepath, product.created)
        )
        # Save changes (basically actually execute the insert query)
        db.commit()
        # Return True if everything is good. (if not, then it will throw an exception)
        return product
    # In case of error, just return False
    except Exception as e:
        print(e)
        return None


def create_product_by_properties(id: str, title: str, username: str, brandname:str, category: str, content: str,price:int,created: str, picturepath: str ="" ):
    """Create a prodcut in the database by individual properties. Returns `True` if successful, `False` if it failed."""
    product = Product (
            id = id,
            title = title,
            username = username,
            brandname = brandname,
            category= category,
            content = content,
            price = price,
            picturepath = picturepath,
            created = created
    )#TODO change
    if create_product(product):
        return product
    else:
        return None

def change_product_picture_path(product_id: str, new_picture_path: str):
    """Change user's picture path. Returns `True` if successful, `False` if it failed."""
    try:
        # Connect to database
        db = Db._db_connect(common.DB_NAME)
        # Get the cursor (that will execute the query)
        cur = db.cursor()
        # Execute query with the values from the User object.
        cur.execute("""UPDATE products
                    SET picture_path = ?
                    WHERE id = ?;
                    """,
                    (new_picture_path, product_id))
        # Save changes (basically actually execute the insert query)
        db.commit()
        # Return True if everything is good. (if not, then it will throw an exception)
        return True
    # In case of error, just return False
    except Exception as e:
        print(e)
        return False

def change_product_content(product_id: str, new_content: str):
    """Update the content of a product. Returns `True` if successful, `False` if it failed."""
    try:
        # Connect to database
        db = Db._db_connect(common.DB_NAME)
        # Get the cursor (that will execute the query)
        cur = db.cursor()
        # Execute query with the values from the details. We update the user with 'username'.
        cur.execute(
            """UPDATE products
                    SET
                        content = ?
                    WHERE id = ?;
                    """,
            (new_content, product_id),
        )
        # Save changes (basically actually execute the insert query)
        db.commit()
        # Return True if everything is good. (if not, then it will throw an exception)
        return True
    # In case of error, just return False
    except Exception as e:
        print(e)
        return False
    
def change_product_title(product_id: str, new_title: str):
    """Update the content of a product. Returns `True` if successful, `False` if it failed."""
    try:
        # Connect to database
        db = Db._db_connect(common.DB_NAME)
        # Get the cursor (that will execute the query)
        cur = db.cursor()
        # Execute query with the values from the details. We update the user with 'username'.
        cur.execute(
            """UPDATE products
                    SET
                        title = ?
                    WHERE id = ?;
                    """,
            (new_title, product_id),
        )
        # Save changes (basically actually execute the insert query)
        db.commit()
        # Return True if everything is good. (if not, then it will throw an exception)
        return True
    # In case of error, just return False
    except Exception as e:
        print(e)
        return False

def change_product_price(product_id: str, new_price: str):
    """Update the content of a product. Returns `True` if successful, `False` if it failed."""
    try:
        # Connect to database
        db = Db._db_connect(common.DB_NAME)
        # Get the cursor (that will execute the query)
        cur = db.cursor()
        # Execute query with the values from the details. We update the user with 'username'.
        cur.execute(
            """UPDATE products
                    SET
                        price = ?
                    WHERE id = ?;
                    """,
            (new_price, product_id),
        )
        # Save changes (basically actually execute the insert query)
        db.commit()
        # Return True if everything is good. (if not, then it will throw an exception)
        return True
    # In case of error, just return False
    except Exception as e:
        print(e)
        return False
    
def change_product_brandname(product_id: str, new_brandname: str):
    """Update the content of a product. Returns `True` if successful, `False` if it failed."""
    try:
        # Connect to database
        db = Db._db_connect(common.DB_NAME)
        # Get the cursor (that will execute the query)
        cur = db.cursor()
        # Execute query with the values from the details. We update the user with 'username'.
        cur.execute(
            """UPDATE products
                    SET
                        brandname = ?
                    WHERE id = ?;
                    """,
            (new_brandname, product_id),
        )
        # Save changes (basically actually execute the insert query)
        db.commit()
        # Return True if everything is good. (if not, then it will throw an exception)
        return True
    # In case of error, just return False
    except Exception as e:
        print(e)
        return False

def change_product_category(product_id: str, new_category: str):
    """Update the content of a product. Returns `True` if successful, `False` if it failed."""
    try:
        # Connect to database
        db = Db._db_connect(common.DB_NAME)
        # Get the cursor (that will execute the query)
        cur = db.cursor()
        # Execute query with the values from the details. We update the user with 'username'.
        cur.execute(
            """UPDATE products
                    SET
                        category = ?
                    WHERE id = ?;
                    """,
            (new_category, product_id),
        )
        # Save changes (basically actually execute the insert query)
        db.commit()
        # Return True if everything is good. (if not, then it will throw an exception)
        return True
    # In case of error, just return False
    except Exception as e:
        print(e)
        return False

def delete_product(product_id: str):
    """Delete a product. Returns `True` if successful, `False` if it failed."""
    try:
        # Connect to database
        db = Db._db_connect(common.DB_NAME)
        # Get the cursor (that will execute the query)
        cur = db.cursor()
        # Execute query with the values from the details. We update the user with 'username'.
        cur.execute(
            """DELETE FROM products
                    WHERE id = ?;
                    """,
            (product_id,),
        )
        # Save changes (basically actually execute the insert query)
        db.commit()
        # Return True if everything is good. (if not, then it will throw an exception)
        return True
    # In case of error, just return False
    except Exception as e:
        print(e)
        return False

def search_products(q, page, results_per_page):
        try:
            conn = Db._db_connect(common.DB_NAME)
            cursor = conn.cursor()

            # First, get the total count of results for pagination metadata
            cursor.execute("""SELECT COUNT(*) FROM products WHERE title LIKE ? OR username LIKE ? OR brandname LIKE ? OR category LIKE ?""",
                            ('%' + q + '%', '%' + q + '%', '%' + q + '%', '%' + q + '%'))
            total_results = cursor.fetchone()[0]
            total_pages = (total_results + results_per_page - 1) // results_per_page

            # Adjust the SQL query to include LIMIT and OFFSET for pagination
            cursor.execute("""SELECT * FROM products WHERE title LIKE ? OR username LIKE ? OR brandname LIKE ? OR category LIKE ? ORDER BY created DESC LIMIT ? OFFSET ?""",
                           ('%' + q + '%', '%' + q + '%', '%' + q + '%', '%' + q + '%', results_per_page, (page - 1) * results_per_page))
            results = cursor.fetchall()

            products = results

            return products, total_pages

        except Exception as e:
            raise e

        finally:
            conn.close()