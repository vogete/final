from designgarage.models.passwordresetcode import Passwordresetcode
import designgarage.common as common
import designgarage.db.db as Db

def get_code_for_user_by_username(userid: str):
    """Get all products for a single User. Returns a `List[product]` object."""
    try:
        # Connect to database
        db = Db._db_connect(common.DB_NAME)
        # Create a cursor that will execute a query
        cur = db.cursor()
        # Execute query (using the cursor)
        cur.execute(
            "SELECT * FROM passwordresetcodes WHERE userid=?;", (userid,)
        )
        # Fetch all data (from the cursor)
        results = cur.fetchall()

        # If there are no results, we just return an empty List
        if not results:
            print("No results found")
            return None

        prc = Passwordresetcode(
            id=results[0]["id"],
            userid=results[0]["userid"],
            code=results[0]["code"],
        )
        return prc

    # If any error happened, print the error and return None.
    except Exception as e:
        print(e)
        return None
    
def create_passwordresetcode(userid:str, code:str):
    """Create a product in the database based on a `product` object. Returns `True` if successful, `False` if it failed."""
    try:
        # Connect to database
        db = Db._db_connect(common.DB_NAME)
        # Get the cursor (that will execute the query)
        cur = db.cursor()
        # Execute query with the values from the User object.
        cur.execute(
            "INSERT INTO passwordresetcodes VALUES (NULL, ?, ?)",
            (userid, code),
        )
        #change the products model
        # Save changes (basically actually execute the insert query)
        db.commit()
        # Return True if everything is good. (if not, then it will throw an exception)
        return True
    # In case of error, just return False
    except Exception as e:
        print(e)
        return False

def delete_passwordresetcode_by_userid(userid: str):
    """Delete a product. Returns `True` if successful, `False` if it failed."""
    try:
        # Connect to database
        db = Db._db_connect(common.DB_NAME)
        # Get the cursor (that will execute the query)
        cur = db.cursor()
        # Execute query with the values from the details. We update the user with 'username'.
        cur.execute(
            """DELETE FROM passwordresetcodes
                    WHERE userid = ?;
                    """,
            (userid,),
        )
        # Save changes (basically actually execute the insert query)
        db.commit()
        # Return True if everything is good. (if not, then it will throw an exception)
        return True
    # In case of error, just return False
    except Exception as e:
        print(e)
        return False