"""
Database queries that is related to Brands.

This file interacts directly with the database, so if you need to fo any database interaction, you do it here.
"""

# All these imported modules are coded in this project
from designgarage.models.brand import Brand
import designgarage.db.db as Db
import designgarage.common as common

def get_brands():
    """Get all brands. Returns a `List[brand]` object."""
    try:
        # Connect to database
        db = Db._db_connect(common.DB_NAME)
        # Create a cursor that will execute a query
        cur = db.cursor()
        # Execute query (using the cursor)
        cur.execute("SELECT * FROM brands")
        # Fetch all data (from the cursor)
        results = cur.fetchall()

        # If there are no results, we just return an empty List
        if not results:
            print("No results found")
            return []

        # Create an empty list where we store all results
        brands = []
        # Go through all results, and create a brand (from `/models/brand.py`) object
        for result in results:
            # Creating brand object
            brand = brand (
                id = result["id"],
                brandname = result["brandname"],
                description = result["description"],
                picturepath = result['picture_path'],
                created = result["created"],
            )
            # Add this brand to the List
            brands.append(brand)

        # Return the list of brands
        return brands

    # If any error happened, then just return an empty list, and print the error
    except Exception as e:
        print(e)
        return []

def get_brand_by_brandname(brandname: str):
    """Get a single brand by brandname. Returns a `brand` object."""
    try:
        db = Db._db_connect(common.DB_NAME)
        cur = db.cursor()
        # Run query
        cur.execute("SELECT * FROM brands WHERE brandname=?", (brandname,))
        # Only fetch one single result, because brandnames should be unique.
        result = cur.fetchone()

        # If there are no results, we just return None
        if not result:
            print(f"No results found for brand {brandname}")
            return None

        # Create a brand object with all the details
        brand = Brand (
            id = result["id"],
            brandname = result["brandname"],
            description = result["description"],
            picturepath = result['picture_path'],
            created = result["created"],
            )
        # Return the brand object
        return brand

    # If any error happened, print the error and return None.
    except Exception as e:
        print(e)
        return None

def get_brand_by_id(id: str):
    """Get a single brand by Email. Returns a `brand` object."""
    try:
        db = Db._db_connect(common.DB_NAME)
        cur = db.cursor()
        # Run query
        cur.execute("SELECT * FROM brands WHERE id=?", (id,))
        # Only fetch one single result, because id should be unique.
        result = cur.fetchone()

        # If there are no results, we just return None
        if not result:
            print(f"No results found for brand {id}")
            return None

        # Create a brand object with all the details
        brand = Brand (
            id = result["id"],
            brandname = result["brandname"],
            description = result["description"],
            picturepath = result['picture_path'],
            created = result["created"],
            )
        # Return the brand object
        return brand

    # If any error happened, print the error and return None.
    except Exception as e:
        print(e)
        return None

def create_brand(brand: Brand):
    """Create a brand in the database based on a `brand` object. Returns `True` if successful, `False` if it failed."""
    try:
        # Connect to database
        db = Db._db_connect(common.DB_NAME)
        # Get the cursor (that will execute the query)
        cur = db.cursor()
        # Execute query with the values from the brand object.
        cur.execute("INSERT INTO brands VALUES (?, ?, ?, ?, ?)", (brand.id, brand.brandname,  brand.description, brand.picturepath, brand.created))
        # Save changes (basically actually execute the insert query)
        db.commit()
        # Return True if everything is good. (if not, then it will throw an exception)
        return True
    # In case of error, just return False
    except Exception as e:
        print(e)
        return False


def change_brand_details(id: str, brandname: str, description: str, picturepath: str):
    """Update brand details. Returns `True` if successful, `False` if it failed."""
    try:
        # Connect to database
        db = Db._db_connect(common.DB_NAME)
        # Get the cursor (that will execute the query)
        cur = db.cursor()
        # Execute query with the values from the details. We update the brand with 'brandname'.
        cur.execute("""UPDATE brands
                    SET
                        brandname = ?,
                        description = ?,
                        picturepath = ?
                    WHERE id = ?;
                    """,
                    (brandname, description, picturepath, id))
        # Save changes (basically actually execute the insert query)
        db.commit()
        # Return True if everything is good. (if not, then it will throw an exception)
        return True
    # In case of error, just return False
    except Exception as e:
        print(e)
        return False


def change_brand_picture_path(id: str, brandname: str, new_picture_path: str):
    """Change brand's picture path. Returns `True` if successful, `False` if it failed."""
    try:
        # Connect to database
        db = Db._db_connect(common.DB_NAME)
        # Get the cursor (that will execute the query)
        cur = db.cursor()
        # Execute query with the values from the brand object.
        cur.execute("""UPDATE brands
                    SET picture_path = ?
                    WHERE id = ?;
                    """,
                    (id, new_picture_path, brandname))
        # Save changes (basically actually execute the insert query)
        db.commit()
        # Return True if everything is good. (if not, then it will throw an exception)
        return True
    # In case of error, just return False
    except Exception as e:
        print(e)
        return False