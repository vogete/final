from datetime import datetime
import random
import sqlite3
import uuid
import lipsum

# All these imported modules are coded in this project
import designgarage.db.db_users as Db_users
import designgarage.db.db_products as Db_products
import designgarage.common


def create_json_from_sqlite_result(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d

def _db_connect(db_name):
    db = sqlite3.connect(db_name)
    #db.row_factory = create_json_from_sqlite_result
    db.row_factory = sqlite3.Row  # Allows us to access columns by name
    return db

def initialize_database():
    """
    Set up the database for usage. Create tables, and if necessary, even populate them.

    This could be done in a separate SQL file too, but if we do it here, then we don't need to manually do anything.
    """

    db = _db_connect(designgarage.common.DB_NAME)
    cur = db.cursor()

    # Create Users table
    cur.execute('''CREATE TABLE IF NOT EXISTS users
                (
                    "id" TEXT NOT NULL PRIMARY KEY,
                    "username" TEXT NOT NULL UNIQUE,
                    "firstname" TEXT,
                    "lastname" TEXT,
                    "email" TEXT NOT NULL UNIQUE,
                    "password" TEXT,
                    "created" TEXT NOT NULL,
                    "picture_path" TEXT
                );
                ''')
    #create brands table
    cur.execute('''CREATE TABLE IF NOT EXISTS brands
                (
                    "id" TEXT NOT NULL PRIMARY KEY,
                    "brandname" TEXT NOT NULL UNIQUE,
                    "description" TEXT,
                    "picture_path" TEXT,
                    "created" TEXT NOT NULL
                );
                ''')
    # Create Products table
    cur.execute('''CREATE TABLE IF NOT EXISTS products
                (
                    "id" TEXT NOT NULL PRIMARY KEY,
                    "title" TEXT NOT NULL,
                    "username" TEXT NOT NULL,
                    "brandname" TEXT, 
                    "category" TEXT,
                    "content" TEXT,
                    "price" INTEGER,
                    "picture_path" TEXT,
                    "created" TEXT NOT NULL,
                    FOREIGN KEY(username) REFERENCES users(username)
                );
                ''')
    # Create junction table for many to many relationship
    cur.execute('''CREATE TABLE IF NOT EXISTS userbrand
                (
                    "userid" TEXT NOT NULL,
                    "brandid" TEXT NOT NULL,
                    PRIMARY KEY (userid, brandid)
                );
                ''')
    # Save (commit) the changes
    cur.execute('''CREATE TABLE IF NOT EXISTS passwordresetcodes
                (
                    "id" INTEGER PRIMARY KEY,
                    "userid" TEXT NOT NULL UNIQUE,
                    "code" TEXT NOT NULL
                );
                ''')

    # Create indexes for faster search functionality
    cur.execute('''CREATE INDEX IF NOT EXISTS idx_products ON products(title, username, brandname, category)''')

    db.commit()

    db.close()

# Create some test data for development

def create_dummy_data():
    """Create some dummy data for easier testing. If the data is already there, don't create it"""
    print("created dummy")
    if not Db_users.get_user_by_username("admincompany"):
        Db_users.create_user_by_properties(str(uuid.uuid1()), "admincompany", "admin", "admin", "admin@designgarage.com", "admin", datetime.now().strftime("%Y-%B-%d-%A %H:%M:%S"), "")
        for i in range(5):
            Db_users.create_user_by_properties(
                str(uuid.uuid1()),
                lipsum.generate_words(1)+str(i),
                lipsum.generate_words(2),
                lipsum.generate_words(1),
                lipsum.generate_words(1)+"@"+lipsum.generate_words(1)+str(random.randint(0,1000))+".com",
                "admin",
                datetime.now().strftime("%Y-%B-%d-%A %H:%M:%S"),
                ""
            )
    else:
        print("Admincomapny is already in the database")

    
    if not Db_products.get_product_by_brandname("gubi"):
        Db_products.create_product_by_properties(str(uuid.uuid1()),"sofa1","seller1", "gubi", "sofa", "240cm red, used on photoshoot", 1234, datetime.now().strftime("%Y-%B-%d-%A %H:%M:%S"),"")
        
        list_of_brands=[]
        for i in range(10):
            brand=lipsum.generate_words(1)+str(random.randint(1,1000))
            list_of_brands.append(brand)

        users=Db_users.get_users()

        for i in range(20):
            Db_products.create_product_by_properties(
                str(uuid.uuid1()),
                lipsum.generate_words(random.randint(1,3)),
                users[random.randint(0,len(users)-1)].username,
                list_of_brands[random.randint(0,len(list_of_brands)-1)],
                lipsum.generate_words(1),
                lipsum.generate_paragraphs(1),
                random.randint(1000,1000000),
                datetime.now().strftime("%Y-%B-%d-%A %H:%M:%S"),
                "",
            )
    else:
        print("gubi stock is already in the database")

    print("Dummy data successfully created")