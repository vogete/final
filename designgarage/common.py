from datetime import datetime
import json
import os
import random
import string
# using SendGrid's Python Library
# https://github.com/sendgrid/sendgrid-python
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail
import re
import uuid
import hashlib
from flask import Flask
from flask_bcrypt import Bcrypt

app = Flask(__name__)
bcrypt = Bcrypt(app)

API_PREFIX = "api"

REGEX_EMAIL = '^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$'
REGEX_PASSWORD = "^.{4,}$"
REGEX_USERNAME = "^[a-zA-Z0-9_-]{4,20}$"
REGEX_UUID4 = "^[0-9a-f]{8}\b-[0-9a-f]{4}\b-[0-9a-f]{4}\b-[0-9a-f]{4}\b-[0-9a-f]{12}$"

JWT_SECRET = "9OzAdFhiJ44vUzK5ikTlflOgztgi45yft3C7VTK6ND2mTEhl9a"
JWT_COOKIE = "user_session_jwt"

# I put it in the database folder because docker wanted it.
DB_NAME = "database/database.sqlite"


def passwordhash(password: str):
    """making string from hash object, putting it into hashedpw and return it"""
    hashed_pw = bcrypt.generate_password_hash(password).decode('utf-8')
    return hashed_pw

def check_password(hashed_pw, candidate_pw):
    return bcrypt.check_password_hash(hashed_pw, candidate_pw)

'''def passwordhash(password: str):
    """making string from hash object, putting it into hashedpw and return it"""
    h = hashlib.new('sha256')
    h.update(bytes(password, "utf-8"))
    hashedpw = h.hexdigest()
    return hashedpw'''

def generate_uuid():
    krumpli = str(uuid.uuid4())
    return krumpli

def generate_created_at():
    krumpli = datetime.now().strftime("%Y-%B-%d-%A %H:%M:%S")
    return krumpli

def load_secrets():
    filename = "secrets.json"
    if not os.path.isfile(filename):
        raise FileNotFoundError(f"Secretfile does not exist.")
    with open(filename, "r") as json_file:
        try:
            # Load JSON data from the file
            data = json.load(json_file)
            return data
        except json.JSONDecodeError as e:
            print(f"Error loading JSON: {e}")


def send_mail(to_address, subject, plain_text_content = None, html_content = None):
    SECRETS = load_secrets() #this is the stupidest thing i ever done in my life after starting webdev but it makes it work because it will open the file every time i send an email

    content = ""
    if plain_text_content:
        content = plain_text_content
    elif html_content:
        content = html_content

    message = Mail(
        from_email='designgarage@pinkbananastudio.dk',
        to_emails=to_address,
        subject=subject,
        html_content=content)
    try:
        print(SECRETS)
        sg = SendGridAPIClient(SECRETS['sendgrid_api_key'])
        response = sg.send(message)
        print(response.status_code)
        print(response.body)
        print(response.headers)
    except Exception as e:
        print(e)


def generate_randomcode(length=16):
    characters = string.ascii_letters + string.digits
    code = ''.join(random.choice(characters) for _ in range(length))
    return code
