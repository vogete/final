"""Users model. It's used to help create User objects everywhere else."""

class User():
    """This is a user object. All users has to have the same structure as this object."""
    def __init__(self, 
                id:str = "", 
                username: str ="", 
                firstname: str = "", 
                lastname: str="", 
                email: str ="",
                password: str= "", 
                created: str="",
                picturepath: str = "",
    ):
        
        self.id = id 
        self.username = username
        self.firstname = firstname
        self.lastname = lastname
        self.email = email
        self.password = password
        self.created = created
        self.picturepath = picturepath
        
        self.is_authenticated = True
        self.is_active = True
        self.is_anonymous = False
        
    def get_id(self):
        return self.id
