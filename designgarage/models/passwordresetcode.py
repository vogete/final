"""product model. It's used to help create product objects everywhere else."""
class Passwordresetcode():
    """This is a product object. All products has to have the same structure as this object."""
    def __init__(self, 
                id:int = -1,
                userid: str ="",
                code:str ="",
    ):
        
        self.id = id 
        self.userid = userid
        self.code = code