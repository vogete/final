"""Brand model. It's used to help create brand objects everywhere else."""
class Brand():
    """This is a brand object. All brands has to have the same structure as this object."""
    def __init__(self, 
                id:str = "", 
                brandname: str ="",
                description: str ="",
                picturepath: str = "",                
                created: str="",
    ):
        
        self.id = id
        self.brandname = brandname
        self.description = description
        self.picturepath = picturepath
        self.created = created