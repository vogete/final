"""product model. It's used to help create product objects everywhere else."""
class Product():
    """This is a product object. All products has to have the same structure as this object."""
    def __init__(self,
                id:str = "",
                title:str ="",
                username: str ="",
                brandname: str ="",
                category: str ="",
                content: str ="",
                price: int =0,
                picturepath: str = "",
                created: str="",
    ):
        
        self.id = id 
        self.title = title
        self.username = username
        self.brandname = brandname
        self.category = category
        self.content = content
        self.price = price
        self.picturepath = picturepath
        self.created = created
