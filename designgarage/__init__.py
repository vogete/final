from flask import Flask
from flask_bcrypt import Bcrypt
from flask_login import LoginManager
import designgarage.db.db_users as db_users


app = Flask(__name__)
bcrypt = Bcrypt(app)
login_manager = LoginManager(app)
login_manager.login_view = 'login'
login_manager.login_message_category = 'info' #bootstrap class for making it pretty
app.config['SECRET_KEY'] = '5791628bb0b13ce0c676dfde280ba666'


@login_manager.user_loader
def load_user(user_id):
    return db_users.get_user_by_id(user_id)