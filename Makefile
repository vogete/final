up:
	docker compose up -d designgarage nginx

down:
	docker compose down

debug:
	docker compose up designgarage-dev
