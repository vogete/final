# Final

## Creating `secrets.json`

Create a `secrets.json` file based on the `secrets.json.template` and put in the secret(s) you need. Never ever share the real secrets to anyone!

## Running locally with Python and VENV

Running the program (from PowerShell on Windows):

Create a new Python VENV:

```
Remove-Item './.venv' -Recurse
mkdir .venv
python -m venv .venv
```

Activate the Python VENV:

```
.venv/Scripts/Activate.ps1
```

Install all Python packages from the requirements.txt file (all python packages you need should be in this file)

```
pip3 install -r requirements.txt
```

Run the application using the VENV Python app.

```
python run.py
```

## Running with Docker

Running in development mode:

```sh
docker compose up designgarage-dev
```

or in linux with make:

```sh
make debug
```

Running in production mode (Not working yet, under development):

```sh
docker compose up designgarage nginx
```

# Debugging tips in VSCode

- F5: start debugger
- ctrl+space: code autocomplete
- ctrl+shift+space: display hints
