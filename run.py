from designgarage.routes.authentication import setup_auth_routes
from designgarage.routes.pages import setup_page_routes
from designgarage.routes.products import setup_product_routes
from designgarage.routes.users import setup_user_routes

from designgarage import app
from werkzeug.middleware.proxy_fix import ProxyFix
import designgarage.db.db as db
from designgarage import routes, common #pylint: disable=unused-import

def method_name():
    pass
db.initialize_database()
db.create_dummy_data()

setup_page_routes(app)
setup_auth_routes(app)
setup_product_routes(app)
setup_user_routes(app)
try:
    # Try to import production settings
    #from production import app as production_app
    #app = production_app
    print("Running in production mode.")
except ImportError:
    # If production settings not found, run in development mode
    print("Running in development mode.")
    if __name__ == "__main__":
        app.run(debug=True)

# Use ProxyFix middleware for reverse proxy setups
app.wsgi_app = ProxyFix(app.wsgi_app)

if __name__ == "__main__":
    app.run(debug=True, use_reloader=True, host='0.0.0.0', port=5000)